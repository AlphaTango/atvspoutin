/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	ATVpoutIn/main.CPP			2016 AlphaTangoVideo				WEB: https://alphatangovideo.wordpress.com/
//	v0.5a
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include	"main.h"
//#include	"..\SpoutSDK\Source\Spout.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	The Developer ID identifes the developer of the module, i.e. you. Set this to whatever 
//	valid hex number you like, and re-use this for all your modules. Some of these are
//	already in use: 0x50000000 0xE4EC7600 and 0x11111112 are used internally by Elektronika
//	and I use 0x60000000 and 0xa1fa7a60 (alfatago)
//	The Module IDs identify each component of your module and have to be unique, across
//	_all_ modules for your chosen Developer ID - so these musn't be re-used.
//
//															 Dev. ID   , Module ID
ACI	AatvSpoutInInfo::CI		= ACI("AatvSpoutInInfo",	GUID(0xa1fa7a60, 0x0000000D), &AeffectInfo::CI, 0, NULL);
ACI	AatvSpoutIn::CI			= ACI("AatvSpoutIn",		GUID(0xa1fa7a60, 0x0000000E), &Aeffect::CI, 0, NULL);
ACI	AatvSpoutInFront::CI	= ACI("AatvSpoutInFront",	GUID(0xa1fa7a60, 0x0000000F), &AeffectFront::CI, 0, NULL);
ACI	AatvSpoutInBack::CI		= ACI("AatvSpoutInBack",	GUID(0xa1fa7a60, 0x00000010), &AeffectBack::CI, 0, NULL);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static int	count = 0;	// optional, used by init() to track how many instances of your module are loaded

//static Aresource	resdll=Aresource("atvSpoutIn", GetModuleHandle("ATVSpoutIn.dll"));
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// optional, used to update global variables shared by all loaded instances of your module
static void init()
{
	count++;	// keep track of the number of instances of this module that are loaded
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// optional, used to update global variables shared by all loaded instances of your module
static void end()
{
	count--;	// decrement the number of instances of this module
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// constructor
AatvSpoutIn::AatvSpoutIn(QIID qiid, char *name, AeffectInfo *info, Acapsule *capsule) : Aeffect(qiid, name, info, capsule)
{
	init();																	// optional

	spoutinput = null;	// types.h in Alib sets null = 0, wrong type!												// initialise tmp bitmap to null for now

	front=new AatvSpoutInFront(qiid, "atvSpoutIn front", this, 64);		// create the front side of your module
	front->setTooltips("Spout Input");										// optional, set the tooltip for your module front
	back=new AatvSpoutInBack(qiid, "atvSpoutIn back", this, 64);		// create the back side of your module
	back->setTooltips("Spout Input");										// optional, set the tooltip for your module back

	spoutreceiver	= new SpoutReceiver;	// Create a Spout receiver object
	bInitialized	= false;				// Spout receiver initialization
	bOglInit		= false;				// Open GL initialisation 
	SenderName[0]	= 0;					// the name will be filled when the receiver connects to a sender
	senderbuffer	= NULL;

	settings(false);					// update our settings (i.e. set tmp bitmap)

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// destructor
AatvSpoutIn::~AatvSpoutIn()
{
	if (spoutinput)
		delete (spoutinput);
	if (senderbuffer)
		delete (senderbuffer);
	if (spoutreceiver){
		spoutreceiver->spout.interop.CloseOpenGL();
		delete (spoutreceiver);
	}
	end();	//optional, global free - needs to be last/after delete()
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// save your module settings into .eka file when user clicks 'save' button
bool AatvSpoutIn::load(class Afile *f)
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// load module settings  from .eka file when user clicks 'load' button
bool AatvSpoutIn::save(class Afile *f)
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// optional, apparently called if table->videoW or table->videoH have been changed 
void AatvSpoutIn::settings(bool emergency)
{
	g_Width = getVideoWidth();				// get output video width
	g_Height = getVideoHeight();				// get output video height

	width = g_Width;
	height = g_Height;

	if(spoutinput)								// if the tmp bitmap already exists (i.e. we pressed master play, but then change project resolution)
		spoutinput->size(g_Width, g_Height);		// set it to video width, height
	else										// otherwise
		spoutinput=new Abitmap(g_Width, g_Height);				// create tmp bitmap and set it to video width, height

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AatvSpoutIn::actionStart(double time){

	section.enter(__FILE__,__LINE__);

	if (!bOglInit){		// if OpenGL is not initialised
				if(spoutreceiver->spout.interop.InitOpenGL()){	// initialise OpenGL
					bOglInit = true; // OpenGL is initialised
					front->asyncNotify(this, nyERROR, (dword)"OpenGL initialised");
				}
	}
	
//	senderbuffer = (unsigned char *)malloc(width*height*4*sizeof(unsigned char));	// allocate a buffer that the receiver can write the image to
//	if(senderbuffer) free((void *)senderbuffer);	// if the buffer alread exists, clear the buffer

	settings(true);
	bInitialized = false;

//	SenderName[0]	= 0;		//testing 		// the name will be filled when the receiver connects to a sender

	section.leave();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// called by Elektronika once per frame
void AatvSpoutIn::action(double time, double dtime, double beat, double dbeat)
{
	AatvSpoutInFront	*front = (AatvSpoutInFront *)this->front;	// reference to the front of the module, so that we can access UI controls
	Avideo			*out = ((AatvSpoutInBack *)back)->out;		// reference to the Output pin at the back of the module
	out->enter(__FILE__,__LINE__);									// Critical Section - lock the output pin
	{
		Abitmap	*bo = out->getBitmap();								// get the output bitmap
		if(bo) {														// verify output bitmap exists
			
			if(!bInitialized) {	// if the receiver isn't initialised
				if(spoutreceiver->CreateReceiver(SenderName, width, height, true)) {	// try to create a receiver		--=== ISSUE IS WITH SENDERNAME BEING 'BLANK' AFTER PLAY/PAUSE TOGGLE

					if(senderbuffer) free((void *)senderbuffer);	// if the buffer alread exists, clear the buffer - it shouldn't exist though...
					senderbuffer = (unsigned char *)malloc(width*height*4*sizeof(unsigned char));	// allocate a buffer that the receiver can write the image to

					if(width != g_Width || height != g_Height ) {	// if the sender dimensions have changed
						g_Width  = width;	// update the local width and height						
						g_Height = height;

						//spoutinput = new Abitmap(g_Width, g_Height);	//
						spoutinput->size(g_Width, g_Height);	// Update the local texture to receive the new dimensions
						spoutinput->boxf(0, 0, 512, 64, bitmapGREEN);	// debug output
					}

					bInitialized = true;	// the receiver is now initialised
					spoutinput->boxf(0, 0, 512, 64, bitmapDARKGRAY);
					bo->set(0,0,spoutinput);
					goto jmpend;	// quit for the next round
				}

				else {	// the receiver couldn't be created
					spoutinput->boxf(0, 0, 512, 64, bitmapYELLOW);
					if(wglGetCurrentContext() == NULL) {				// if context was lost, e.g.: master stop button was pressed. Works when not connected, 
						spoutinput->boxf(0, 0, 512, 64, bitmapRED);		// but crash still occurs if pplay/pause dtoggled once connected.
						spoutreceiver->ReleaseReceiver();	// release the receiver and try again (does this make sense here?! receiver wasn't created...)
						bInitialized = false;
						bOglInit = false;

						free((void *)senderbuffer);

						spoutreceiver->spout.interop.CloseOpenGL();		// put before ReleaseReceiver?

						actionStart(0);									// incase module was added after Master Play button waspressed
					}
					bo->set(0,0,spoutinput);
					goto jmpend;	// quit for the next round
				}

			}

			if(bInitialized) {	// the receiver is initalised

				width  = g_Width;	// Save current global width and height - they will be changed
				height = g_Height;	// by ReceiveTexture if the sender changes dimensions

				if(spoutreceiver->ReceiveImage(SenderName, width, height, senderbuffer, GL_BGRA_EXT)) {	// try to receive an image
					// Otherwise transfer spoutbuffer to your plugin image
					if(width != g_Width || height != g_Height ) {	// has the size of image being received changed?
						g_Width  = width; // Update the global width and height
						g_Height = height;

						free((void *)senderbuffer);	// clear buffer
						senderbuffer = (unsigned char *)malloc(width*height*4*sizeof(unsigned char));	// reset buffer to new size

						spoutinput->size(g_Width, g_Height);	// Update the local texture to receive the new dimensions
						goto jmpend;	// quit for the next round
					}

					// the size of the received image is still the same
					spoutinput->boxf(0, 0, 512, 64, bitmapMAGENTA);
					int		ww = g_Width;	// store the image size
					int		hh = g_Height;

					int		i;
					int		yd = 0;	// destination 'pointer'
					int		ys = 0;	// source 'pointer'
					for(i=0; i<hh; i++)
					{
						memcpy(&spoutinput->body32[yd], &senderbuffer[ys], ww*4*sizeof(byte)/* <<2 */);	// copy one line of pixels from the buffer to the Abitmap
						//ys += spoutinput->w;
						//yd += width;
						ys += g_Width*4;		//	update source 'pointer' to the next line
						yd += spoutinput->w;	// update destination pointer to the next line
					}		
					bo->set(0, 0, spoutinput, bitmapNORMAL, bitmapNORMAL);	// update the output bitmap
				}	
				else {	// couldn't receive the image
					spoutreceiver->ReleaseReceiver();	// A texture read failure might happen if the sender is closed. Release the receiver and start again.
					bInitialized = false;

					free((void *)senderbuffer);	// free the buffer?

					spoutinput->boxf(0, 0, 512, 64, bitmapWHITE);	//crashed here if the sender is renamed
					bo->set(0,0,spoutinput);
					
					//actionStop();	// start again.. - didn't like that... crashes if sender is closed. call earlier? some duplicate calls in this block and actionStop()

					//out->leave();													// Critical Section - unlock the output pin (which we locked first so unlock last)
				}	// Spout stuff - ends
				//bo->set(0, 0, spoutinput, bitmapNORMAL, bitmapNORMAL);			// set the output bitmap with value from the tmp bitmap
			}
		}
	}
	//	in->leave();													// Critical Section - unlock the input pin (which we locked most recently)
jmpend:
	out->leave();													// Critical Section - unlock the output pin (which we locked first so unlock last)
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AatvSpoutIn::actionStop(){

	section.enter(__FILE__,__LINE__);

	spoutreceiver->spout.interop.CloseOpenGL();		//these don't prevent a crash when toggling play/pause :|
	bOglInit = false;

	front->asyncNotify(this, nyERROR, (dword)"OpenGL closed");

	spoutreceiver->ReleaseReceiver();	
	bInitialized = false;
	
//	free((void *)senderbuffer);	// free the buffer?

	// delete[] SenderName;	// delete data pointed to by char* // causes crash
	//SenderName[0] = NULL;

	section.leave();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// optional? update the front of the module with a given bitmao
void AatvSpoutInFront::paint(Abitmap *b)
{
	b->set(0, 0, back, bitmapDEFAULT, bitmapDEFAULT);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// constructor for module front
AatvSpoutInFront::AatvSpoutInFront(QIID qiid, char *name, AatvSpoutIn *e, int h) : AeffectFront(qiid, name, e, h)
{
	back=new Abitmap(512,64);	//backgroung img
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// destructor for module front
AatvSpoutInFront::~AatvSpoutInFront()
{
	delete(back);	// delete backgroung img
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// handle user input for module front
bool AatvSpoutInFront::notify(Anode *o, int event, dword p)
{
	switch(event)
	{
	case nyCHANGE:
		break;

		/*
		case nyPRESS:
		if((o==lauto)||(o==rauto))
		{
			if(((Avmix *)effect)->getTable()->isRunning())
			{
				dirauto=(o==lauto)?-1:1;
				beatauto=effect->getBeat();
			}
		}
		break;
		*/

	}
	return AeffectFront::notify(o, event, p);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// same as front
AatvSpoutInBack::AatvSpoutInBack(QIID qiid, char *name, AatvSpoutIn *e, int h) : AeffectBack(qiid, name, e, h)
{
//	Aresobj	o=resdll.get(MAKEINTRESOURCE(IDB_PNG6), "PNG");
	back=new Abitmap(512,64);

	//	qiid's need to be unique for _all_ controls in elektronika - use randomGuid.exe to make new ones (clicking 'gen' copies them to the clipboard)
	out=new Avideo(MKQIID(qiid, 0x1bada8eac8a7c12f),  "video output", this, pinOUT, pos.w-18, 10);	// video iutput pin
	out->setTooltips("video output");
	out->show(TRUE);
	/*
	in=new Avideo(MKQIID(qiid, 0x4bd2a71deb4521e4), "video input", this, pinIN, 10, 10);			//video input pin
	in->setTooltips("video input");
	in->show(TRUE);
	*/

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// same as front
bool AatvSpoutInBack::notify(Anode *o, int event, dword p){

/*	if (event == nyCONTEXT)
	{
		Aitem *i=(Aitem *)p;
		if(i->data == contextCLEAR){
			ShellExecute(getWindow()->hw, "open", "https://alphatangovideo.wordpress.com/", NULL, NULL, SW_SHOWNORMAL);
			return true;
		}
	}*/
	return AeffectBack::notify(o, event, p);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// same as front
AatvSpoutInBack::~AatvSpoutInBack()
{
	delete(out);
//	delete(in);
	delete(back);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// same as front
void AatvSpoutInBack::paint(Abitmap *b)
{
	b->set(0, 0, back, bitmapDEFAULT, bitmapDEFAULT);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// destructor for moduleInfo
AatvSpoutInInfo::~AatvSpoutInInfo()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// create moduleInfo
Aeffect * AatvSpoutInInfo::create(QIID qiid, char *name, Acapsule *capsule)
{
	return new AatvSpoutIn(qiid, name, this, capsule);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// export our functions via DLL?
extern "C" 
{
	DLLEXPORT class Aplugz * getPlugz()
	{								//module name						//name in list	//tooltip in list
		return new AatvSpoutInInfo("atvSpoutIn", &AatvSpoutIn::CI, "Spout Input", "Spout Input");
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////